package com.frame.compiler.utils.viewPage;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * project:PJHAndroidFrame
 * package:com.frame.pjhandroidframe.base
 * Created by 彭俊鸿 on 2018/5/23.
 * e-mail : 1031028399@qq.com
 * 复用的adapter
 */

public abstract class BaseViewPagerAdapter extends FragmentStatePagerAdapter {

    private int fragmentsSize;

    public BaseViewPagerAdapter(FragmentManager fm, int fragmentsSize) {
        super(fm);
        this.fragmentsSize = fragmentsSize;
    }

    @Override
    public int getCount() {
        return fragmentsSize;
    }

    @Override
    public abstract Fragment getItem(int arg0);

    @Override
    public void finishUpdate(@NonNull ViewGroup container) {
        try{
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException){
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
}
