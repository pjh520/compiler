package com.frame.compiler.utils.wheel;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.frame.compiler.R;
import com.frame.compiler.widget.ToastUtil;
import com.frame.compiler.widget.customPop.BottomDialogUtils;
import com.frame.compiler.widget.wheel.WheelView;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：dgonline-android
 * 包    名：com.aten.compiler.widget.customerDialog
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/10/29
 * 描    述：城市选择wheel（省市区联动）
 * ================================================
 */
public class CityWheelUtils<T1,T2,T3> {
    private BottomDialogUtils bottomDialogUtils;
    private String cityInfo = "";
    private WheelView<T1> wvProvince;
    private WheelView<T2> wvCity;
    private WheelView<T3> wvArea;

    //显示城市选择框
    public void showCityWheel(Context context, final CityWheelClickListener cityWheelClickListener) {
        View cityWheelView = View.inflate(context, R.layout.layout_city_wheel_choose, null);
        ArrayList<WheelView> wvs = new ArrayList<>();
        wvProvince = (WheelView) cityWheelView.findViewById(R.id.wv_province);
        wvCity = (WheelView) cityWheelView.findViewById(R.id.wv_city);
        wvArea = (WheelView) cityWheelView.findViewById(R.id.wv_area);
        wvs.add(wvProvince);
        wvs.add(wvCity);
        wvs.add(wvArea);
        setWvOption(context, wvs);

        wvProvince.setCurvedArcDirection(WheelView.CURVED_ARC_DIRECTION_LEFT);
        wvProvince.setCurvedArcDirectionFactor(0.65f);
        wvArea.setCurvedArcDirection(WheelView.CURVED_ARC_DIRECTION_RIGHT);
        wvArea.setCurvedArcDirectionFactor(0.65f);

        wvProvince.setOnItemSelectedListener(new WheelView.OnItemSelectedListener<T1>() {
            @Override
            public void onItemSelected(WheelView<T1> wheelView, T1 data, int position) {
                cityWheelClickListener.onCity(data);
            }
        });

        wvCity.setOnItemSelectedListener(new WheelView.OnItemSelectedListener<T2>() {
            @Override
            public void onItemSelected(WheelView<T2> wheelView, T2 data, int position) {
                cityWheelClickListener.onArea(data);
            }
        });

        bottomDialogUtils = new BottomDialogUtils(context);
        bottomDialogUtils.showBottomDialogDialog(cityWheelView, "地址", new BottomDialogUtils.BottomClickListener() {
            @Override
            public void onSure() {
                cityWheelClickListener.onchooseCity(wvProvince == null ? null : wvProvince.getSelectedItemPosition(), wvProvince == null ? null : wvProvince.getSelectedItemData(),
                        wvCity == null ? null : wvCity.getSelectedItemPosition(), wvCity == null ? null : wvCity.getSelectedItemData(),
                        wvArea == null ? null : wvArea.getSelectedItemPosition(), wvArea == null ? null : wvArea.getSelectedItemData());
            }

            @Override
            public void onCancle() {
            }
        });
    }

    //设置wheelview的属性
    private void setWvOption(Context context, ArrayList<WheelView> wvs) {
        for (WheelView wv : wvs) {
            wv.setVisibleItems(7);
            wv.setAutoFitTextSize(true);
            wv.setSelectedRectColor(Color.parseColor("#1e1e1e"));
            wv.setNormalItemTextColor(Color.parseColor("#808080"));
            wv.setTextSize(18f, true);
            wv.setShowDivider(true);
            wv.setDividerType(WheelView.DIVIDER_TYPE_FILL);
            wv.setDividerColor(context.getResources().getColor(R.color.line02));
            wv.setDividerPaddingForWrap(10, true);
            wv.setDividerHeight(0.5f, true);
            wv.setResetSelectedPosition(true);

            wv.setLineSpacing(15, true);
        }
    }

    //设置省的数据
    public void setProvinceData(List<T1> provinceDatas) {
        if (wvProvince == null) {
            ToastUtil.show("数据有误,请重新打开页面！");
        } else {
            wvProvince.setData(provinceDatas);
        }
    }

    //设置市的数据
    public void setCityData(List<T2> cityDatas) {
        if (wvCity == null) {
            ToastUtil.show("数据有误,请重新打开页面！");
        } else {
            wvCity.setData(cityDatas);
        }
    }

    //设置区的数据
    public void setAreaData(List<T3> areaDatas) {
        if (wvArea == null) {
            ToastUtil.show("数据有误,请重新打开页面！");
        } else {
            wvArea.setData(areaDatas);
        }
    }

//    //设置省的初始数据
//    public void setProvinceData(String cityId,List<AddressCityModel.DataBean> areaDatas){
//        int cityPos=-1;
//        for (int i = 0; i < areaDatas.size(); i++) {
//            if (cityId.equals(areaDatas.get(i).getAreaId())){
//                cityPos=i;
//            }
//        }
//
//        if (cityPos!=-1&&wvProvince!=null){
//            wvProvince.setSelectedItemPosition(cityPos,true);
//        }
//    }
//
//    //设置记忆选中的数据
//    public void setSelectPos(int provincePos,int cityPos,int areaPos){
//        wvProvince.setSelectedItemPosition(provincePos);
//        wvCity.setSelectedItemPosition(cityPos);
//        wvArea.setSelectedItemPosition(areaPos);
//    }
//
//    //设置市的初始数据
//    public void setCityData(String cityId,List<AddressCityModel.DataBean> areaDatas){
//        int cityPos=-1;
//        for (int i = 0; i < areaDatas.size(); i++) {
//            if (cityId.equals(areaDatas.get(i).getAreaId())){
//                cityPos=i;
//            }
//        }
//
//        if (cityPos!=-1&&wvCity!=null){
//            wvCity.setSelectedItemPosition(cityPos,true);
//        }
//    }
//
//    //设置区的数据
//    public void setAreaData(String cityId,List<AddressCityModel.DataBean> areaDatas){
//        int cityPos=-1;
//        for (int i = 0; i < areaDatas.size(); i++) {
//            if (cityId.equals(areaDatas.get(i).getAreaId())){
//                cityPos=i;
//            }
//        }
//
//        if (cityPos!=-1&&wvArea!=null){
//            wvArea.setSelectedItemPosition(cityPos,true);
//        }
//    }

    public void setProvinceSelectItem(int position) {
        if (wvProvince != null) {
            wvProvince.setSelectedItemPosition(position, true, 1000);
        }
    }

    public void setCitySelectItem(int position) {
        if (wvCity != null) {
            wvCity.setSelectedItemPosition(position, true, 1000);
        }
    }

    public void setAreaSelectItem(int position) {
        if (wvArea != null) {
            wvArea.setSelectedItemPosition(position, true, 1000);
        }
    }

    public interface CityWheelClickListener<T1,T2,T3> {
        void onCity(T1 provinceData);//获取城市以及区的数据

        void onArea(T2 provinceData);//获取区的数据

        void onchooseCity(int provincePos, T1 provinceItemData,
                          int cityPos, T2 cityItemData,
                          int areaPos, T3 areaItemData);
    }

    //清理缓存
    public void onCleanData(){
        if (bottomDialogUtils!=null){
            bottomDialogUtils.dismissBottomDialogDialog();
        }
    }
}