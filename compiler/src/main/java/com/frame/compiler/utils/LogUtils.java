package com.frame.compiler.utils;

import android.util.Log;

public final class LogUtils {
    private static boolean mIsShowLog=false;

    public static void init(boolean isShowLog){
        mIsShowLog=isShowLog;
    }

    //打印日志e
    public static void e(String msg) {
        if (mIsShowLog) {
            e("",msg);
        }
    }

    //打印日志e
    public static void e(String tag, String msg) {
        if (mIsShowLog) {
            Log.e(EmptyUtils.isEmpty(tag) ? "LogUtils" : tag, msg);
        }
    }

    //打印日志d
    public static void d(String tag, String msg) {
        if (mIsShowLog) {
            Log.d(EmptyUtils.isEmpty(tag) ? "LogUtils" : tag, msg);
        }
    }
}