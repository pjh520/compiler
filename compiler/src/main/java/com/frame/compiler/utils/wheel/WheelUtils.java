//package com.frame.compiler.utils.wheel;
//
//import android.content.Context;
//import android.view.View;
//import com.frame.compiler.R;
//import com.frame.compiler.widget.wheel.WheelView;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * ================================================
// * 项目名称：dgonline-android
// * 包    名：com.aten.compiler.widget.customerDialog
// * 作    者：彭俊鸿
// * 邮    箱：1031028399@qq.com
// * 版    本：1.0
// * 创建日期：2018/10/29
// * 描    述：wheel数据选择
// * ================================================
// */
//public class WheelUtils<T> {
//    private BottomDialogUtils bottomDialogUtils;
//
//    //显示单一wheel
//    public  void showWheel(Context context, String title, int choosePos, List<T> datas,
//                          final WheelClickListener wheelClickListener) {
//        View wheelView = View.inflate(context, R.layout.layout_wheel_choose, null);
//        final WheelView<T> wheelview=wheelView.findViewById(R.id.wheelview);
//
//        wheelview.setData(datas);
//        wheelview.setSelectedItemPosition(choosePos);
//        if (bottomDialogUtils==null){
//            bottomDialogUtils = new BottomDialogUtils(context);
//        }
//        bottomDialogUtils.showBottomDialogDialog(wheelView, title, new BottomDialogUtils.BottomClickListener() {
//            @Override
//            public void onSure(BottomDialog bottomDialog) {
//                wheelClickListener.onchooseDate(wheelview.getSelectedItemPosition(),wheelview.getSelectedItemData());
//            }
//
//            @Override
//            public void onCancle(BottomDialog bottomDialog) {
//            }
//        });
//    }
//
//    //显示两列wheel
//    public  void showTwoWheel(Context context, String title, int choosePos01,int choosePos02,
//                              List<T> datas01,List<T> datas02,
//                              final ITwoWheelClickListener iTwoWheelClickListener) {
//        View wheelView = View.inflate(context, R.layout.layout_twowheel_choose, null);
//        final WheelView<T> wheelview01=(WheelView)wheelView.findViewById(R.id.wheelview01);
//        final WheelView<T> wheelview02=(WheelView)wheelView.findViewById(R.id.wheelview02);
//
//        wheelview01.setData(datas01);wheelview02.setData(datas02);
//        wheelview01.setSelectedItemPosition(choosePos01);wheelview02.setSelectedItemPosition(choosePos02);
//
//        if (bottomDialogUtils==null){
//            bottomDialogUtils = new BottomDialogUtils(context);
//        }
//        bottomDialogUtils.showBottomDialogDialog(wheelView, title, new BottomDialogUtils.BottomClickListener() {
//            @Override
//            public void onSure(BottomDialog bottomDialog) {
//                iTwoWheelClickListener.onchooseDate(wheelview01.getSelectedItemData(),wheelview02.getSelectedItemData());
//            }
//
//            @Override
//            public void onCancle(BottomDialog bottomDialog) {
//            }
//        });
//    }
//
//    //显示两列wheel
//    public  void showThreeWheel(Context context, String title, int choosePos01,int choosePos02,int choosePos03,
//                                List<T> datas01,List<T> datas02,List<T> datas03,
//                              final IThreeWheelClickListener iThreeWheelClickListener) {
//        View wheelView = View.inflate(context, R.layout.layout_threewheel_choose, null);
//        final WheelView<T> wheelview01=(WheelView)wheelView.findViewById(R.id.wheelview01);
//        final WheelView<T> wheelview02=(WheelView)wheelView.findViewById(R.id.wheelview02);
//        final WheelView<T> wheelview03=(WheelView)wheelView.findViewById(R.id.wheelview03);
//
//        wheelview01.setData(datas01);wheelview02.setData(datas02);wheelview03.setData(datas03);
//        wheelview01.setSelectedItemPosition(choosePos01);wheelview02.setSelectedItemPosition(choosePos02);
//        wheelview03.setSelectedItemPosition(choosePos03);
//
//        if (bottomDialogUtils==null){
//            bottomDialogUtils = new BottomDialogUtils(context);
//        }
//        bottomDialogUtils.showBottomDialogDialog(wheelView, title, new BottomDialogUtils.BottomClickListener() {
//            @Override
//            public void onSure(BottomDialog bottomDialog) {
//                iThreeWheelClickListener.onchooseDate(wheelview01.getSelectedItemData(),
//                        wheelview02.getSelectedItemData(),wheelview03.getSelectedItemData());
//            }
//
//            @Override
//            public void onCancle(BottomDialog bottomDialog) {
//            }
//        });
//    }
//
//    public void dissWheel() {
//        if (bottomDialogUtils!=null){
//            bottomDialogUtils.dismissBottomDialogDialog();
//            bottomDialogUtils=null;
//        }
//    }
//
//    public interface WheelClickListener<T> {
//        void onchooseDate(int choosePos, T dateInfo);
//    }
//
//    public interface ITwoWheelClickListener<T> {
//        void onchooseDate(T data01,T data02);
//    }
//
//    public interface IThreeWheelClickListener<T> {
//        void onchooseDate(T data01,T data02,T data03);
//    }
//}
