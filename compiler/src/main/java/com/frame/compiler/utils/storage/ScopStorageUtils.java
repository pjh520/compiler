package com.frame.compiler.utils.storage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.RecoverableSecurityException;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import androidx.fragment.app.FragmentActivity;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.utils.storage
 * 版    本：1.0
 * 创建日期：2021/10/19 0019
 * 描    述：androidQ分区存储适配
 * <p>
 * 切换媒体文件的待处理状态
 * 如果您的应用执行可能非常耗时的操作（例如写入媒体文件）,那么在处理文件时对其进行独占访问非常有用。
 * 在搭载 Android 10 或更高版本的设备上，您的应用可以通过将 IS_PENDING 标记的值设为 1 来获取此独占访问权限。如此一来，
 * 只有您的应用可以查看该文件，直到您的应用将 IS_PENDING 的值改回 0,values.put(MediaStore.MediaColumns.IS_PENDING,0);
 * ================================================
 */
public class ScopStorageUtils {
    public static final int TYPE_CREATE_MEDIA_REQUEST = 10001;

    //获取内部共享存储
    public static String getInternalStorageByType(String type) {
        return FileStorageUtils.getInternalFilePath(type);
    }

    //未开启分区存储时,获取外部共享存储
    public static File getExternalSharedStorageByFileMame(String fileName) {
        //1 自动推断媒体文件的类型
        String mimeType = FileStorageUtils.guessExternalFileMimeType(fileName);
        return getExternalSharedStorageFileByMineType(mimeType);
    }

    //未开启分区存储时,获取外部共享存储
    public static String getExternalSharedStorageByMineType(String mimeType) {
        //构建文件存储路径    /storage/emulated/0/Pictures/
        File externalMediaDir = FileStorageUtils.guessExternalFileDirectory(mimeType);
        File externalMediaAppDir = new File(externalMediaDir, FileStorageUtils.EXTERNAL_FILE_DIRECTORY);
        if (!externalMediaAppDir.exists()) {
            externalMediaAppDir.mkdirs();
        }
        return externalMediaAppDir.getAbsolutePath();
    }

    //未开启分区存储时,获取外部共享存储
    public static File getExternalSharedStorageFileByMineType(String mimeType) {
        //构建文件存储路径    /storage/emulated/0/Pictures/
        File externalMediaDir = FileStorageUtils.guessExternalFileDirectory(mimeType);
        //构建文件存储的子目录 /storage/emulated/0/Pictures/imooc
        File externalMediaAppDir = new File(externalMediaDir, FileStorageUtils.EXTERNAL_FILE_DIRECTORY);
        if (!externalMediaAppDir.exists()) {
            externalMediaAppDir.mkdirs();
        }
        return externalMediaAppDir;
    }

    //开启分区存储时,获取uri作为存储路径
    public static Uri getAndroidQUri(String fileName, String mimeType) {
        MediaInfo mediaInfo = querySingleMedias(fileName);
        if (mediaInfo != null) {
            return mediaInfo.getUri();
        }
        // 1. 自动推断媒体文件的类型
        String mediaMimeType = EmptyUtils.isEmpty(mimeType) ? FileStorageUtils.guessExternalFileMimeType(fileName) : mimeType;
        // 2. 推断出媒体文件应该插入的数据库的uri
        Uri externalMediaUri = FileStorageUtils.guessExternalMediaUri(mediaMimeType);

        ContentValues values = new ContentValues();
        // 文件名称
        values.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
        // 文件类型，如果传递的为null, 则自动根据文件名的后缀去推断
        values.put(MediaStore.MediaColumns.MIME_TYPE, mediaMimeType);
        // MediaStore 会根据 Uri 自动存储到对应分类目录下, 并在目录下创建 relativePath 文件夹
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            values.put(MediaStore.Images.Media.RELATIVE_PATH, RxTool.getContext().getPackageName());
        }

        return RxTool.getContext().getContentResolver().insert(externalMediaUri, values);
    }

    //开启分区存储时,删除指定的uri路径图片
    public static boolean deleteAndroidQUri(Uri uri) {
        return RxTool.getContext().getContentResolver().delete(uri, null, null) > 0;
    }

    /**
     * 使用MediaStore API 保存媒体文件(音频，视频，图片)到媒体共享目录(pictures/,movies/,audio/)
     * 启用分区存储:
     * 此方法不需要权限，适用于全版本
     * 未启用分区存储:
     * 需要先申请获取{WRITE_EXTERNAL_STORAGE}
     *
     * @param context   上下文对象
     * @param byteArray 待写入媒体文件的数据
     * @param fileName  媒体文件名称,比如(hello.txt)
     * @param mimeType  媒体文件的类型，比如(text/plain),如果为空则根据{@param fileName}自动推断
     * @param width     媒体文件的宽(图片、视频)
     * @param height    媒体文件的高(图片、视频)
     * @return 文件的uri
     */
    @SuppressLint("InlinedApi")
    public static Uri saveMedia(Context context, byte[] byteArray, String fileName, String mimeType, int width, int height) {
        // 1. 自动推断媒体文件的类型
        String mediaMimeType = EmptyUtils.isEmpty(mimeType) ? FileStorageUtils.guessExternalFileMimeType(fileName) : mimeType;
        // 2. 推断出媒体文件应该插入的数据库的uri
        Uri externalMediaUri = FileStorageUtils.guessExternalMediaUri(mediaMimeType);
        // 3. 分区存储未被启用，即使用运行时动态权限模式,跟Android10之前的文件读写一样
        if (FileStorageUtils.isExternalStorageLegacy()) {
            //4.获取文件存储file
            File ExternalSharedStorageFileDir = getExternalSharedStorageFileByMineType(mediaMimeType);
            // 5. 文件流写入
            File mediaFile = new File(ExternalSharedStorageFileDir, fileName);
            try {
                FileOutputStream outputStream = new FileOutputStream(mediaFile);
                outputStream.write(byteArray);
                outputStream.flush();
                outputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
                mediaFile.delete();
                return null;
            }

            // 6. 插入相册
            ContentValues values = new ContentValues();
            // 文件绝对路径
            values.put(MediaStore.MediaColumns.DATA, mediaFile.getAbsolutePath());
            // 文件名称
            values.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
            // 文件类型，如果传递的为null, 则自动根据文件名的后缀去推断
            values.put(MediaStore.MediaColumns.MIME_TYPE, mediaMimeType);
            // 对于图片，视频媒体文件，可以选择把宽高信息也存储起来
            values.put(MediaStore.MediaColumns.WIDTH, width);
            values.put(MediaStore.MediaColumns.HEIGHT, height);

            Uri uri = null;
            MediaInfo mediaInfo = querySingleMedias(fileName);
            if (mediaInfo != null) {
                uri = context.getContentResolver().update(mediaInfo.getUri(), values, null, null) > 0 ? mediaInfo.getUri() : null;
            } else {
                // 根据文件类型，插入图片或视频或音频的数据库中
                uri = context.getContentResolver().insert(externalMediaUri, values);
            }
            values.clear();
//            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//            intent.setData(Uri.parse("file://" + mediaFile.getAbsolutePath()));
//            context.sendBroadcast(intent);
            return uri;
        } else {
            ContentValues values = new ContentValues();
            // 文件名称
            values.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
            // 文件类型，如果传递的为null, 则自动根据文件名的后缀去推断
            values.put(MediaStore.MediaColumns.MIME_TYPE, mediaMimeType);
            // 对于图片，视频媒体文件，可以选择把宽高信息也存储起来
            values.put(MediaStore.MediaColumns.WIDTH, width);
            values.put(MediaStore.MediaColumns.HEIGHT, height);
            // 获取独占访问权限，在文件写入成功之前，其它应用不可对此文件进行访问，操作
            values.put(MediaStore.MediaColumns.IS_PENDING, 1);
            // 1. 根据文件的类型，去推断是应该插入Image 表，还是Movies 表，还是Audio表
            Uri uri = null;
            MediaInfo mediaInfo = querySingleMedias(fileName);
            if (mediaInfo != null) {
                uri = mediaInfo.getUri();
            } else {
                uri = context.getContentResolver().insert(externalMediaUri, values);
            }

            // 得到创建的空文件的uri 则获取输出流，将bytes数据写入
            OutputStream openOutputStream = null;
            try {
                openOutputStream = context.getContentResolver().openOutputStream(uri);
                openOutputStream.write(byteArray);
                openOutputStream.flush();
                openOutputStream.close();
//                values.clear();
                // 更新文件状态，此时其它应用程序可以访问的到
                values.put(MediaStore.MediaColumns.IS_PENDING, 0);
                context.getContentResolver().update(uri, values, null, null);
                // 很奇怪，在Android 11上自己创建的文件，有的时候也是没权限去操作它的
                // 在这里可以授予自己修改该文件的权限。在{deleteFile}的时候就不会弹窗了
                grantUriPermission(context, uri);
                return uri;
            } catch (Exception e) {
                deleteAndroidQUri(uri);
                e.printStackTrace();
                return null;
            }
        }
    }

    //查询到的媒体文件信息 查询指定文件的media信息
    public static MediaInfo querySingleMedias(String displayName) {
        // 查询语句的where 条件
        String selection = "";
        // 查询语句where条件中的占位符参数值
        ArrayList<String> sectionArgs = new ArrayList<>();
        // 1. 构建查询的列名集合
        String[] projection = {MediaStore.MediaColumns._ID, MediaStore.MediaColumns.DATA,
                MediaStore.MediaColumns.DISPLAY_NAME, MediaStore.MediaColumns.WIDTH,
                MediaStore.MediaColumns.HEIGHT, MediaStore.MediaColumns.MIME_TYPE};
        // 2. 如果查询的文件名不为空，则全匹配查询
        if (!TextUtils.isEmpty(displayName)) {
            selection += MediaStore.MediaColumns.DISPLAY_NAME + " = ? ";
            sectionArgs.add(displayName);
        }
        // 3. 自动推断媒体文件的类型
        String mimeType = FileStorageUtils.guessExternalFileMimeType(displayName);
        // 4. 推断要查询的媒体文件的uri
        //如果查询所有媒体文件，则uri格式为content://media/external/file/
        Uri mediaExternalUri = FileStorageUtils.guessExternalMediaUri(mimeType);
//        Log.e("ScopStorageActivity","mimeType==="+mimeType);
//        Log.e("ScopStorageActivity","mediaExternalUri==="+mediaExternalUri.toString());
//        Log.e("ScopStorageActivity","selection==="+selection);
//        Log.e("ScopStorageActivity","sectionArgs==="+sectionArgs.toString());
        Cursor cursor = RxTool.getContext().getContentResolver().query(mediaExternalUri, projection, selection, (String[]) sectionArgs.toArray(new String[sectionArgs.size()]), null);

        MediaInfo mediaInfo = null;
        if (cursor != null && cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(MediaStore.MediaColumns._ID);
            int pathIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
            int displayNameIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME);
            int widthIndex = cursor.getColumnIndex(MediaStore.MediaColumns.WIDTH);
            int heightIndex = cursor.getColumnIndex(MediaStore.MediaColumns.HEIGHT);
            int mimeTypeIndex = cursor.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE);
            //图片类型的uri格式：content://media/external/images/media/
            //视频类型的uri格式：content://media/external/video/media/
            Uri externalMediaUri = FileStorageUtils.guessExternalMediaUri(cursor.getString(mimeTypeIndex));

            mediaInfo = new MediaInfo(
                    //content://media/external/video/media/21
                    ContentUris.withAppendedId(externalMediaUri, cursor.getLong(idIndex)),
                    cursor.getString(displayNameIndex),
                    cursor.getString(mimeTypeIndex),
                    cursor.getInt(widthIndex),
                    cursor.getInt(heightIndex),
                    cursor.getString(pathIndex));
//            try {
//                //判断文件是否有数据
//                AssetFileDescriptor afd = RxTool.getContext().getContentResolver().openAssetFileDescriptor(ContentUris.withAppendedId(externalMediaUri, cursor.getLong(idIndex)), "r");
//                if (afd != null){
//                    mediaInfo=new MediaInfo(
//                            //content://media/external/video/media/21
//                            ContentUris.withAppendedId(externalMediaUri, cursor.getLong(idIndex)),
//                            cursor.getString(displayNameIndex),
//                            cursor.getString(mimeTypeIndex),
//                            cursor.getInt(widthIndex),
//                            cursor.getInt(heightIndex),
//                            cursor.getString(pathIndex));
//                }
//                afd.close();
//            } catch (IOException ignore) {
//            }
        }

        cursor.close();
        return mediaInfo;
    }

    /**
     * 使用MediaStore API 查询外部共享存储空间的媒体文件(音频，视频，图片)。
     * 已启用分区存储:
     * ————————如果已获得{@READ_EXTERNAL_STORAGE}权限，则可以查询设备上所有媒体文件。但私有目录、非媒体目录下存放的媒体文件查询不到
     * ————————如果没有获得{READ_EXTERNAL_STORAGE}权限，则只能查询到属于自己应用的媒体文件
     * 未启用分区存储:
     * ————————需要先申请获取{@READ_EXTERNAL_STORAGE}权限
     * 此方法适用于全版本
     * a. 如果{@param displayName }不为空，则返回文件名称为{@param displayName }的媒体信息
     * b. 如果{@param mimeType }不为空，则在指定的文件类型中去查询文件名为{@param displayName}的媒体信息
     * c. 如果{@param displayName }，{@param mimeType }同时为空，则查询所有媒体文件
     *
     * @param displayName 待查询的文件名称，比如(hello.txt)
     * @param mimeType    待查询的文件的类型，比如(text/plain)
     * @return 查询到的媒体文件信息
     */
    public static List<MediaInfo> queryMedias(Activity activity, String displayName, String mimeType) {
        List list = new ArrayList<MediaInfo>();
        // 查询语句的where 条件
        String selection = "";
        // 查询语句where条件中的占位符参数值
        ArrayList<String> sectionArgs = new ArrayList<>();
        // 1. 构建查询的列名集合
        String[] projection = {MediaStore.MediaColumns._ID, MediaStore.MediaColumns.DATA,
                MediaStore.MediaColumns.DISPLAY_NAME, MediaStore.MediaColumns.WIDTH,
                MediaStore.MediaColumns.HEIGHT, MediaStore.MediaColumns.MIME_TYPE};
        // 2. 如果查询的文件名不为空，则全匹配查询
        if (!TextUtils.isEmpty(displayName)) {
            selection += MediaStore.MediaColumns.DISPLAY_NAME + " = ? ";
            sectionArgs.add(displayName);
        }
        // 3. 如果文件类型不为空，则半匹配查询
        mimeType = EmptyUtils.isEmpty(mimeType) ? FileStorageUtils.guessExternalFileMimeType(displayName) : mimeType;

        if (!TextUtils.isEmpty(mimeType)) {
            if (!EmptyUtils.isEmpty(selection)) {
                selection += "and ";
            }
            selection += MediaStore.MediaColumns.MIME_TYPE + " LIKE ?";
            sectionArgs.add(mimeType);
        }
//        // 4. 如果文件类型未指定，则查询image、video、audio所有媒体文件
//        if (TextUtils.isEmpty(mimeType)) {
//            if (!EmptyUtils.isEmpty(selection)) {
//                selection += "and ";
//            }
//            selection += MediaStore.MediaColumns.MIME_TYPE + " LIKE ? " +
//                    "or " + MediaStore.MediaColumns.MIME_TYPE + " LIKE ? " +
//                    "or " + MediaStore.MediaColumns.MIME_TYPE + " LIKE ?";
//            sectionArgs.add("image%");
//            sectionArgs.add("video%");
//            sectionArgs.add("audio%");
//        }
        // 5. 查询结果的排序方式，以ID 倒叙排
        String sortOrder = MediaStore.MediaColumns._ID + " DESC";
        // 6. 推断要查询的媒体文件的uri
        //如果查询所有媒体文件，则uri格式为content://media/external/file/
        Uri mediaExternalUri = FileStorageUtils.guessExternalMediaUri(mimeType);

        //根据指定条件 查询媒体库
        Cursor cursor = activity.getContentResolver().query(mediaExternalUri, projection, selection,
                (String[]) sectionArgs.toArray(new String[sectionArgs.size()]), sortOrder);
        while (cursor.moveToNext()) {
            int idIndex = cursor.getColumnIndex(MediaStore.MediaColumns._ID);
            int pathIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
            int displayNameIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME);
            int widthIndex = cursor.getColumnIndex(MediaStore.MediaColumns.WIDTH);
            int heightIndex = cursor.getColumnIndex(MediaStore.MediaColumns.HEIGHT);
            int mimeTypeIndex = cursor.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE);

            MediaInfo media = new MediaInfo(
                    ContentUris.withAppendedId(mediaExternalUri, cursor.getLong(idIndex)),
                    cursor.getString(displayNameIndex),
                    cursor.getString(mimeTypeIndex),
                    cursor.getInt(widthIndex),
                    cursor.getInt(heightIndex),
                    cursor.getString(pathIndex));
            list.add(media);
        }
        cursor.close();
        return list;
    }

    /**
     * 使用MediaStore API 删除文件(图片，视频，音频，文档).
     * 此方法适用于全版本
     * 如果APP targetSDKVersion>29且设备版本>Android10且开启了分区存储，删除不属于自己应用的文件会弹框请求用户授权
     * 如果APP targetSDKVersion<29或设备版本<Android10或未开启分区存储，删除文件需要申请运行时权限{@see android.permission.WRITE_EXTERNAL_STORAGE}
     *
     * @param activity        上下文对象
     * @param uri             待删除的文件的uri
     * @param deleteOptionListener 用于接收文件删除的结果
     */
    public static void deleteFile(FragmentActivity activity, Uri uri, final IDeleteOptionListener deleteOptionListener) {
        // 如果是通过SAF选择的文件/download，/document目录的文档文件【pdf,txt,png】
        // 其uri格式为content://com.android.providers.media.documents/primary:Pictures/android-shared-image.png
        if (DocumentsContract.isDocumentUri(activity, uri)) {
            boolean ret = false;
            try {
                ret = DocumentsContract.deleteDocument(activity.getContentResolver(), uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            deleteOptionListener.onDeleteStatus(ret);
            return;
        }
        // 如果通过MediaStore api 查询得到的文件，
        // 其uri 格式为content://media/external/file/10086
        // 根据文件的uri 推断其类型
        String mimeType = FileStorageUtils.queryMimeTypeFromUri(activity, uri);
        // 判断是不是媒体文件类型
        boolean isMediaFile = FileStorageUtils.isMediaMimeType(mimeType);
        // 是否有权限删除它
        boolean hasPermission = activity.checkUriPermission(uri, Binder.getCallingPid(), Binder.getCallingUid(),
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION) == PackageManager.PERMISSION_GRANTED;
        if (isMediaFile && !hasPermission && Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && !FileStorageUtils.isExternalStorageLegacy()) {
            // android 11上如果没有权限删除该文件，我们才调用createDeleteRequest。才会弹窗
            // 删除的是自己应用的文件，不需要弹窗申请
            //【是媒体文件】且【没有权限删除它】且【android 11的系统】且【开启分区存储】

            // android11上 新增批量操作媒体文件的API
            ArrayList<Uri> uris = new ArrayList<>();
            uris.add(uri);
            PendingIntent pendingIntent = MediaStore.createDeleteRequest(activity.getContentResolver(), uris);
            // 弹窗向用户申请删除文件的权限
            StartActivityUtils.startIntentSenderForResult(activity, pendingIntent.getIntentSender(), TYPE_CREATE_MEDIA_REQUEST, new StartActivityUtils.SimpleCallback() {
                @Override
                public void onResult(Boolean result) {
                    deleteOptionListener.onDeleteStatus(result);
                }
            });
        } else if (isMediaFile && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && !FileStorageUtils.isExternalStorageLegacy()) {
            // 这里不需要判断有木有删除该文件的权限，因为没有权限的话，会走到catch里面，有权限的话直接就删掉了
            // 【是媒体文件】且【android 10系统】且 【开启分区存储】
            try {
                int delete = activity.getContentResolver().delete(uri, null, null);
                deleteOptionListener.onDeleteStatus(delete > 0);
            } catch (Exception ex) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && ex instanceof RecoverableSecurityException) {
                    // 捕获异常，如果是RecoverableSecurityException，则弹窗向用户申请授权
                    // activity.startIntentSender(pendingIntent.intentSender,null,0,0,0)
                    StartActivityUtils.startIntentSenderForResult(activity, ((RecoverableSecurityException) ex).getUserAction().getActionIntent().getIntentSender(), TYPE_CREATE_MEDIA_REQUEST, new StartActivityUtils.SimpleCallback() {
                        @Override
                        public void onResult(Boolean result) {
                            deleteOptionListener.onDeleteStatus(result);
                        }
                    });
                    return;
                } else {
                    ex.printStackTrace();
                    deleteOptionListener.onDeleteStatus(false);
                }
            }
        } else {
            // android 10以前需要申请运行时的write_external_storage权限
            int delete = activity.getContentResolver().delete(uri, null, null);
            deleteOptionListener.onDeleteStatus(delete > 0);
        }
    }

    /**
     * 使用MediaStore API 保存一个非媒体文件(pdf，txt,apk,word,excel...)到外部存储，返回文件保存成功后的uri。文件将存储在/Download/imooc目
     * 如果已开启分区存储，则不需要权限
     * 如果未启用分区存储，则需要先申请获得{ WRITE_EXTERNAL_STORAGE}
     *
     * @param context   上下文对象
     * @param byteArray 待写入文件的数据
     * @param fileName  待创建文件的名称,比如(hello.txt)
     * @param mimeType  待创建文件的类型,比如(text/plain), 如果为空，则根据{@link fileName}自动推断
     * @return 文件创建成功后文件的uri会回调给用户，进而可以写入数据。如果文件创建失败，则uri为空
     */
    @SuppressLint("InlinedApi")
    public static Uri saveDocument(Context context, byte[] byteArray, String fileName, String mimeType) {
        // 自动推断文档文件的类型
        String docMimeType = EmptyUtils.isEmpty(mimeType) ? "" : FileStorageUtils.guessExternalFileMimeType(fileName);
        // 推断出文档发文件应该插入的数据库的uri
        Uri docExternalUri = FileStorageUtils.guessExternalMediaUri(docMimeType);
        // 在使用MediaStore API存储文件时判断一把，媒体文件应该存放在共享媒体目录或应用私有目录
        if (FileStorageUtils.isMediaMimeType(docMimeType)) {
            throw new IllegalArgumentException("Media files should be stored in a shared media directory or application private directory");
        }
        // 分区存储未被启用，使用兼容存储模式，即运行时存储动态权限模式
        if (FileStorageUtils.isExternalStorageLegacy()) {
            // /storage/0/Document
            File externalDocDir = FileStorageUtils.guessExternalFileDirectory(docMimeType);
            // /storage/emulated/0/Document/imooc
            File externalDocAppDir = new File(externalDocDir, FileStorageUtils.EXTERNAL_FILE_DIRECTORY);
            if (!externalDocAppDir.exists()) {
                externalDocAppDir.mkdirs();
            }
            File docFile = new File(externalDocAppDir, fileName);
            try {
                FileOutputStream outputStream = new FileOutputStream(docFile);
                outputStream.write(byteArray);
                outputStream.flush();
                outputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
                docFile.delete();
                return null;
            }

            // 插入文档文件的数据库
            ContentValues values = new ContentValues();
            // 文件绝对路径
            values.put(MediaStore.Files.FileColumns.DATA, docFile.getAbsolutePath());
            // 文件名称
            values.put(MediaStore.Files.FileColumns.DISPLAY_NAME, fileName);
            // 文件类型，如果传递的为null, 则自动根据文件名的后缀去推断
            values.put(MediaStore.Files.FileColumns.MIME_TYPE, docMimeType);
            // 根据文件类型，插入文档文件数据库中
            return context.getContentResolver().insert(docExternalUri, values);
        } else {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Files.FileColumns.DISPLAY_NAME, fileName);
            values.put(MediaStore.Files.FileColumns.MIME_TYPE, docMimeType);
            // 获取独占访问权限，在文件写入成功之前，其它应用不可对此文件进行访问，操作
            values.put(MediaStore.MediaColumns.IS_PENDING, 1);

            Uri uri = context.getContentResolver().insert(FileStorageUtils.DOCUMENT_EXTERNAL_URI, values);
            OutputStream openOutputStream = null;
            try {
                openOutputStream = context.getContentResolver().openOutputStream(uri);
                openOutputStream.write(byteArray);
                openOutputStream.flush();
                openOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            values.clear();
            // 写入成功之后，更新文件的状态
            values.put(MediaStore.MediaColumns.IS_PENDING, 0);
            context.getContentResolver().update(uri, values, null, null);
            // 授予自己修改此文件的权限
            grantUriPermission(context, uri);
            return uri;
        }
    }

    /**
     * 使用MediaStore API查询所有符合条件的文档文件(不包含图片，音频，视频)
     * 自己应用创建的文件有操作权限，对非自己应用创建的文件没有操作权限
     * 开启分区存储后，只能查询到自己应用创建的文档文件
     *
     * @param activity    上下文对象
     * @param displayName 查询的文件的名称
     * @param mimeType    查询的文件的类型
     * @return
     */
    public static List<MediaInfo> queryDocument(Activity activity, String displayName, String mimeType) {
        List list = new ArrayList<MediaInfo>();
        // 查询语句的where 条件
        String selection = "";
        // 查询语句where条件中的占位符参数值
        ArrayList<String> sectionArgs = new ArrayList<>();
        // 构建查询的列名集合
        String[] projection = {MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.DISPLAY_NAME, MediaStore.Files.FileColumns.WIDTH,
                MediaStore.Files.FileColumns.HEIGHT, MediaStore.Files.FileColumns.MIME_TYPE};
        // 如果查询的文件名不为空，则全匹配查询
        if (!TextUtils.isEmpty(displayName)) {
            selection += MediaStore.Files.FileColumns.DISPLAY_NAME + " = ? ";
            sectionArgs.add(displayName);
        }
        // 如果文件类型不为空，则半匹配查询
        if (!TextUtils.isEmpty(mimeType)) {
            selection += MediaStore.Files.FileColumns.MIME_TYPE + " LIKE ?";
            sectionArgs.add(mimeType);
        }
        // 如果文件类型未指定，则查询除了媒体文件之外的所有文档文件
        if (TextUtils.isEmpty(mimeType)) {
            selection += MediaStore.Files.FileColumns.MIME_TYPE + " NOT LIKE ? AND " +
                    MediaStore.Files.FileColumns.MIME_TYPE + " NOT LIKE ? AND " +
                    MediaStore.Files.FileColumns.MIME_TYPE + " NOT LIKE ?";
            sectionArgs.add("image%");
            sectionArgs.add("video%");
            sectionArgs.add("audio%");
        }
        // 查询结果的排序方式，以ID 倒叙排
        String sortOrder = MediaStore.Files.FileColumns._ID + " DESC";
        Cursor cursor = activity.getContentResolver().query(FileStorageUtils.DOCUMENT_EXTERNAL_URI, projection, selection, (String[]) sectionArgs.toArray(new String[sectionArgs.size()]), sortOrder);
        while (cursor.moveToNext()) {
            int idIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns._ID);
            int pathIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
            int displayNameIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DISPLAY_NAME);
            int widthIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.WIDTH);
            int heightIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.HEIGHT);
            int mimeTypeIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE);

            MediaInfo media = new MediaInfo(
                    ContentUris.withAppendedId(FileStorageUtils.DOCUMENT_EXTERNAL_URI, cursor.getLong(idIndex)),
                    cursor.getString(displayNameIndex),
                    cursor.getString(mimeTypeIndex),
                    cursor.getInt(widthIndex),
                    cursor.getInt(heightIndex),
                    cursor.getString(pathIndex)
            );
            list.add(media);
        }
        cursor.close();
        return list;
    }

    /**
     * 授予自己应用修改该文件的权限，这样在android 11上我们删除自己应用创建文件的时，就不用弹窗了
     */
    private static void grantUriPermission(Context activity, Uri uri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            activity.grantUriPermission(FileStorageUtils.getPackageName(), uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
    }

    //判断文件是否存在
    public static boolean isFileExists(File file) {
        if (file != null && file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    //判断文件是否存在AndroidQ及以上
    public static boolean isAndroidQUriExists(Uri uri) {
        AssetFileDescriptor afd = null;
        try {
            afd = RxTool.getContext().getContentResolver().openAssetFileDescriptor(uri, "r");
            if (afd != null) {
                afd.close();
                return true;
            } else {
                afd.close();
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public interface IDeleteOptionListener {
        void onDeleteStatus(boolean isSuccess);
    }

    public interface ISaveOptionListener {
        void onSaveStatus(boolean isSuccess);
    }
}
