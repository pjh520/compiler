package com.frame.compiler.widget;

import android.view.Gravity;
import com.coder.zzq.smartshow.toast.SmartToast;
import com.coder.zzq.toolkit.Utils;
import com.frame.compiler.R;

/**
 * ================================================
 * 项目名称：AndroidFrameNew
 * 包    名：com.frame.compiler.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/5/5
 * 描    述：
 * ================================================
 */
public class ToastUtil {
    //判断是否有通知权限
    public static boolean isNotificationEnabled() {
        return Utils.isNotificationPermitted();
    }

    //显示短时提示
    public static void show(String tip) {
        SmartToast.classic()
                .config()
                //设置当前页面退出时，新进入的页面不会依旧显示还未消失的Toast，默认false
                .cancelOnActivityExit(false)
//                .transition(true)
                .apply()
                .showAtLocation(tip, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,0, 40);
    }

    //显示长时提示
    public static void showLong(String tip) {
        SmartToast.classic()
                .config()
                //设置当前页面退出时，新进入的页面不会依旧显示还未消失的Toast，默认false
                .cancelOnActivityExit(false)
//                .transition(true)
                .apply()
                .showLongAtLocation(tip, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,0, 40);
    }

    //显示中间提示
    public static void showCenter(String tip) {
        SmartToast.classic()
                .config()
                //设置当前页面退出时，新进入的页面不会依旧显示还未消失的Toast，默认false
                .cancelOnActivityExit(false)
//                .transition(true)
                .backgroundColorResource(R.color.colorBackground)
                .msgColorResource(R.color.white)
                .apply()
                .showLongInCenter(tip);
    }
}
