package com.frame.compiler.widget.customPop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.frame.compiler.R;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.adapter.BottomChooseAdapter;
import java.util.ArrayList;
import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/12/11 0011
 * 描    述：底部弹框-带列表
 * ================================================
 */
public class BottomListPop<T> extends BasePopupWindow {
    private String title;
    private ArrayList<String> datas;
    private T otherData;
    private int choosePos;
    private IOptionListener iOptionListener;

    public BottomListPop(Context context, String title, ArrayList<String> datas, T otherData, int choosePos, IOptionListener iOptionListener) {
        super(context);
        this.title=title;
        this.datas=datas;
        this.otherData=otherData;
        this.choosePos=choosePos;
        this.iOptionListener=iOptionListener;
        setContentView(R.layout.layout_bottom_pop);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);

        TextView tvDialogTitle=contentView.findViewById(R.id.tv_dialog_title);
        RecyclerView rlBottomChoose=contentView.findViewById(R.id.rl_bottom_choose);
        TextView stvDialogCancle=contentView.findViewById(R.id.tv_dialog_cancle);
        //设置标题
        if (EmptyUtils.isEmpty(title)){
            tvDialogTitle.setVisibility(View.GONE);
        }else {
            tvDialogTitle.setVisibility(View.VISIBLE);
            tvDialogTitle.setText(title);
        }
        //初始化列表数据
        rlBottomChoose.setHasFixedSize(true);
        rlBottomChoose.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        BottomChooseAdapter adapter = new BottomChooseAdapter(datas,choosePos);
        rlBottomChoose.setAdapter(adapter);
        //设置按钮点击事件
        stvDialogCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        adapter.setmOnBottomChooseItemListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                String str = (String) view.getTag(R.id.tag_1);
                int position = (int) view.getTag(R.id.tag_2);
                iOptionListener.onItemClick(str,position,otherData);
            }
        });
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }

    public interface IOptionListener<T>{
        void onItemClick(String str, int positoion,T otherData);
    }
}
