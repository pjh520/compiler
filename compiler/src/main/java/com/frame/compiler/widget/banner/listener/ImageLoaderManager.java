package com.frame.compiler.widget.banner.listener;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

/**
 * by y on 2016/10/27
 */

public interface ImageLoaderManager<T> {

    @NonNull
    View display(@NonNull ViewGroup container, T model);
}
