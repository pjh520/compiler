package com.frame.compiler.widget.glide;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;
import com.frame.compiler.R;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * project:PJHAndroidFrame
 * package:com.frame.pjh_core.widget.glide
 * Created by 彭俊鸿 on 2018/6/4.
 * e-mail : 1031028399@qq.com
 */

public class GlideUtils {

    //加载网络图片
    public static void loadImg(Object path, ImageView mImageView) {
        loadImg(path, mImageView, R.drawable.ic_placeholder_bg, R.color.color_f0f0f0);
    }

    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView) {
        loadImg(path, mImageView, lodingImage, errorImageView, DiskCacheStrategy.AUTOMATIC, false,null);
    }

    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView,int imgWidth) {
        loadImg(path, mImageView, lodingImage, errorImageView,imgWidth, DiskCacheStrategy.AUTOMATIC, false,null);
    }

    public static void loadImg(Object path, ImageView mImageView, int lodingImage, Transformation requestOptions) {
        loadImg(path, mImageView, lodingImage, lodingImage, DiskCacheStrategy.AUTOMATIC, false,requestOptions);
    }

    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView, DiskCacheStrategy strategy, Transformation requestOptions) {
        loadImg(path, mImageView, lodingImage, errorImageView, strategy, false,requestOptions);
    }

    //needRealtime 是否需要实时获取
    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView, DiskCacheStrategy strategy, boolean needRealtime, Transformation requestOptions) {
        loadImg(path,mImageView,lodingImage,errorImageView,320,strategy,needRealtime,requestOptions);
    }

    //needRealtime 是否需要实时获取
    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView,int imgWidth, DiskCacheStrategy strategy, boolean needRealtime, Transformation requestOptions) {
        if (path instanceof String && (((String) path).contains("http") || ((String) path).contains("https"))) {
            if (((String) path).contains("?")) {
                path = String.valueOf(path) + "&x-oss-process=image/resize,s_"+(imgWidth==0?320:imgWidth);
            } else {
                path = String.valueOf(path) + "?x-oss-process=image/resize,s_"+(imgWidth==0?320:imgWidth);
            }
        }

        if (mImageView == null || mImageView.getContext() == null) {
            return;
        }

        if (mImageView.getContext() instanceof FragmentActivity){
            if (isDestroy((FragmentActivity)mImageView.getContext())){
                return;
            }
        }

        if (needRealtime) {
            if (requestOptions==null){
                GlideApp.with(mImageView.getContext()).load(path).centerCrop().signature(new ObjectKey(UUID.randomUUID())).diskCacheStrategy(strategy).
                        placeholder(lodingImage).error(errorImageView).into(mImageView);
            }else {
                GlideApp.with(mImageView.getContext()).load(path).centerCrop().signature(new ObjectKey(UUID.randomUUID())).diskCacheStrategy(strategy).
                        placeholder(lodingImage).error(errorImageView).transform(new CenterCrop(),requestOptions).into(mImageView);
            }
        } else {
            if (requestOptions==null){
                GlideApp.with(mImageView.getContext()).load(path).centerCrop().diskCacheStrategy(strategy).placeholder(lodingImage).
                        error(errorImageView).into(mImageView);
            }else {
                GlideApp.with(mImageView.getContext()).load(path).centerCrop().diskCacheStrategy(strategy).placeholder(lodingImage).
                        error(errorImageView).transform(new CenterCrop(),requestOptions).into(mImageView);
            }
        }
    }

    //图片加载监听
    public static void loadImg_listener(Object path, ImageView mImageView, int lodingImage, RequestListener<Drawable> listener) {
        if (path instanceof String) {
            if (((String) path).contains("?")) {
                path = String.valueOf(path) + "&x-oss-process=image/resize,s_320";
            } else {
                path = String.valueOf(path) + "?x-oss-process=image/resize,s_320";
            }
        }

        if (mImageView == null || mImageView.getContext() == null) {
            return;
        }

        if (mImageView.getContext() instanceof FragmentActivity){
            if (isDestroy((FragmentActivity)mImageView.getContext())){
                return;
            }
        }

        GlideApp.with(mImageView.getContext()).load(path).centerCrop().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).placeholder(lodingImage).
                error(lodingImage).listener(listener).into(mImageView);
    }

    public static void getImageWidHeig(Context context, String pathUrl, final IGetImageData iGetImageData) {
        if (context==null) {
            return;
        }

        if (context instanceof FragmentActivity){
            if (isDestroy((FragmentActivity)context)){
                return;
            }
        }

        //获取图片真正的宽高
        GlideApp.with(context)
                .asBitmap()//Glide返回一个Bitmap对象
                .load(pathUrl)
                .skipMemoryCache(true)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        double proportionScreen = new BigDecimal(String.valueOf(resource.getWidth())).divide(new BigDecimal(resource.getHeight()), 2, BigDecimal.ROUND_HALF_UP).doubleValue();//屏幕的宽高比
                        iGetImageData.sendData(resource,resource.getWidth(), resource.getHeight(), proportionScreen);
                    }
                });
    }

    public interface IGetImageData {
        void sendData(@NonNull Bitmap resource,int width, int height, double radio);
    }

    /**
     * 判断Activity是否Destroy
     *
     * @param mActivity
     * @return
     */
    public static boolean isDestroy(Activity mActivity) {
        if (mActivity == null || mActivity.isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && mActivity.isDestroyed())) {
            return true;
        } else {
            return false;
        }
    }
}
