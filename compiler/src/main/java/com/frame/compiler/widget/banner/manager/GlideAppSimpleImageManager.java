package com.frame.compiler.widget.banner.manager;

import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.frame.compiler.R;
import com.frame.compiler.widget.banner.listener.BannerModelCallBack;
import com.frame.compiler.widget.banner.listener.ImageLoaderManager;
import com.frame.compiler.widget.glide.GlideApp;

public class GlideAppSimpleImageManager<T extends BannerModelCallBack> implements ImageLoaderManager<T> {

    private final RequestOptions requestOptions;

    public GlideAppSimpleImageManager() {
        requestOptions = new RequestOptions().centerCrop();
    }

    @NonNull
    @Override
    public ImageView display(@NonNull ViewGroup container, T model) {
        ImageView imageView = new ImageView(container.getContext());

        Object path = model.getBannerUrl();
        if (path instanceof String && (((String) path).contains("http") || ((String) path).contains("https"))) {
            if (((String) path).contains("?")) {
                path = String.valueOf(path) + "&x-oss-process=image/resize,s_640";
            } else {
                path = String.valueOf(path) + "?x-oss-process=image/resize,s_640";
            }
        }

        GlideApp.with(imageView.getContext())
                .applyDefaultRequestOptions(requestOptions)
                .load(path)
                .placeholder(R.drawable.ic_placeholder_bg)
                .error(R.drawable.ic_placeholder_bg)
                .fallback(R.drawable.ic_placeholder_bg)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .into(imageView);
        return imageView;
    }
}
