package com.frame.compiler.widget.permission;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.frame.compiler.widget.ToastUtil;
import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;

import java.util.List;

//权限申请帮助类
public class PermissionHelper {

    //申请单个权限的时候用到
    public void requestPermission(final Activity activity, final onPermissionListener onPermissionListener, String permission) {
        XXPermissions.with(activity)
                .permission(permission)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            onPermissionListener.onSuccess();
                        } else {
//                    ToastUtils.show("获取部分权限成功，但部分权限未正常授予");
                            onPermissionListener.onNoAllSuccess();
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            ToastUtil.show("被永久拒绝授权，请到系统设置页面手动授予权限");
                            // 如果是被永久拒绝就跳转到应用权限系统设置页面
                            XXPermissions.startPermissionActivity(activity, permissions);
                        } else {
                            if (permissions.contains(Permission.ACCESS_BACKGROUND_LOCATION)) {
                                ToastUtil.show("没有授予后台定位权限，请您选择\"始终允许\"");
                            } else {
                                ToastUtil.show("获取权限失败");
                            }
                        }

                        onPermissionListener.onFail();
                    }
                });
    }

    //申请多个权限的时候用到
    public void requestPermission(final Activity activity, final onPermissionListener onPermissionListener, List<String> permissions, String[]... permission) {
        XXPermissions xxPermissions = XXPermissions.with(activity);
        if (permissions != null && !permissions.isEmpty()) {
            //申请单个权限
            xxPermissions.permission(permissions);
        }
        if (permission.length != 0) {
            //申请权限组
            xxPermissions.permission(permission);
        }

        xxPermissions.request(new OnPermissionCallback() {
            @Override
            public void onGranted(List<String> permissions, boolean all) {
                if (all) {
                    onPermissionListener.onSuccess();
                } else {
//                    ToastUtils.show("获取部分权限成功，但部分权限未正常授予");
                    onPermissionListener.onNoAllSuccess();
                }
            }

            @Override
            public void onDenied(List<String> permissions, boolean never) {
                if (never) {
                    ToastUtil.show("被永久拒绝授权，请到系统设置页面手动授予权限");
                    // 如果是被永久拒绝就跳转到应用权限系统设置页面
                    XXPermissions.startPermissionActivity(activity, permissions);
                } else {
                    if (permissions.contains(Permission.ACCESS_BACKGROUND_LOCATION)) {
                        ToastUtil.showLong("没有授予后台定位权限，请您选择\"始终允许\"");
                    } else {
                        ToastUtil.show("获取权限失败");
                    }
                }

                onPermissionListener.onFail();
            }
        });
    }

    //申请单个权限的时候用到
    public void requestPermission(final Fragment fragment, final onPermissionListener onPermissionListener, String permission) {
        XXPermissions.with(fragment)
                .permission(permission)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            onPermissionListener.onSuccess();
                        } else {
//                    ToastUtils.show("获取部分权限成功，但部分权限未正常授予");
                            onPermissionListener.onNoAllSuccess();
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            ToastUtil.show("被永久拒绝授权，请到系统设置页面手动授予权限");
                            // 如果是被永久拒绝就跳转到应用权限系统设置页面
                            XXPermissions.startPermissionActivity(fragment, permissions);
                        } else {
                            if (permissions.contains(Permission.ACCESS_BACKGROUND_LOCATION)) {
                                ToastUtil.show("没有授予后台定位权限，请您选择\"始终允许\"");
                            } else {
                                ToastUtil.show("获取权限失败");
                            }
                        }

                        onPermissionListener.onFail();
                    }
                });
    }

    //申请多个权限的时候用到
    public void requestPermission(final Fragment fragment, final onPermissionListener onPermissionListener, List<String> permissions, String[]... permission) {
        XXPermissions xxPermissions = XXPermissions.with(fragment);
        if (permissions != null && !permissions.isEmpty()) {
            //申请单个权限
            xxPermissions.permission(permissions);
        }
        if (permission.length != 0) {
            //申请权限组
            xxPermissions.permission(permission);
        }

        xxPermissions.request(new OnPermissionCallback() {
            @Override
            public void onGranted(List<String> permissions, boolean all) {
                if (all) {
                    onPermissionListener.onSuccess();
                } else {
//                    ToastUtils.show("获取部分权限成功，但部分权限未正常授予");
                    onPermissionListener.onNoAllSuccess();
                }
            }

            @Override
            public void onDenied(List<String> permissions, boolean never) {
                if (never) {
                    ToastUtil.show("被永久拒绝授权，请到系统设置页面手动授予权限");
                    // 如果是被永久拒绝就跳转到应用权限系统设置页面
                    XXPermissions.startPermissionActivity(fragment, permissions);
                } else {
                    if (permissions.contains(Permission.ACCESS_BACKGROUND_LOCATION)) {
                        ToastUtil.showLong("没有授予后台定位权限，请您选择\"始终允许\"");
                    } else {
                        ToastUtil.show("获取权限失败");
                    }
                }

                onPermissionListener.onFail();
            }
        });
    }

    //判断是否具有定位的权限
    public boolean hasHomePermission(Context context) {
        return XXPermissions.isGranted(context, Permission.ACCESS_COARSE_LOCATION, Permission.ACCESS_FINE_LOCATION);
    }

    public interface onPermissionListener {
        void onSuccess();

        void onNoAllSuccess();

        void onFail();
    }
}
//----------- activity需要重新该方法 那样子当权限是从设置页面回来的时候 判断需要的权限已申请
//----------- 那么这时候就可以继续接下去 权限申请成功时的流程---------------------------------
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == XXPermissions.REQUEST_CODE) {
//            toast("检测到你刚刚从权限设置界面返回回来");
//        }
//    }