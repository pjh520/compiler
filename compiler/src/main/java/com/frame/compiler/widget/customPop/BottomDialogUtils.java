package com.frame.compiler.widget.customPop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.frame.compiler.R;
import com.frame.compiler.utils.EmptyUtils;


/**
 * ================================================
 * 项目名称：dgonline-android
 * 包    名：com.aten.compiler.widget.customerDialog
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/10/29
 * 描    述：底部弹框
 * ================================================
 */
public class BottomDialogUtils {
    private Context context;
    private BottomPop bottomDialog;
    private BottomPop customerBottomDialog;

    public BottomDialogUtils(Context context) {
        this.context = context;
    }

    //-------------------------------------------弹出底部框（带取消 确定按钮的）  start-------------------------------------------------
    //(basepopup实现)
    public void showBottomDialogDialog(View contentView, String titleStr, final BottomClickListener bottomClickListener) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View bottomChooseLayout = inflater.inflate(R.layout.layout_bottom_dialog, null);
        TextView cancel = (TextView) bottomChooseLayout.findViewById(R.id.bottomDialog_cancel);
        TextView title = (TextView) bottomChooseLayout.findViewById(R.id.bottomDialog_title);
        TextView ok = (TextView) bottomChooseLayout.findViewById(R.id.bottomDialog_ok);
        FrameLayout customView = (FrameLayout) bottomChooseLayout.findViewById(R.id.bottomDialog_custom_view);

        title.setText(titleStr);
        if (customView.getChildCount() > 0) {
            customView.removeAllViews();
        }
        customView.addView(contentView);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissBottomDialogDialog();
                bottomClickListener.onCancle();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissBottomDialogDialog();
                bottomClickListener.onSure();
            }
        });

        bottomDialog = new BottomPop(context, bottomChooseLayout);
        bottomDialog.showPopupWindow();
    }

    //关闭dialog
    public void dismissBottomDialogDialog() {
        if (bottomDialog != null && bottomDialog.isShowing()) {
            bottomDialog.dismiss();
            bottomDialog = null;
        }
    }

    public interface BottomClickListener {
        void onSure();
        void onCancle();
    }
    //-------------------------------------------弹出底部框（带取消 确定按钮的）  end-------------------------------------------------

    //-------------------------------------------弹出底部框  start-------------------------------------------------
    private BottomPop noPayPwdBottomDialog;

    public void showNoPayPwdDialog(String content, String btnText, final INoPayPwd iNoPayPwd) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View bottomChooseLayout = inflater.inflate(R.layout.layout_nopayment_pwd, null);
        TextView tvContent = bottomChooseLayout.findViewById(R.id.tv_content);
        TextView tvConfirmOpening = bottomChooseLayout.findViewById(R.id.tv_confirm_opening);
        TextView tvCancle = bottomChooseLayout.findViewById(R.id.tv_cancle);

        tvContent.setText(EmptyUtils.strEmpty(content));
        tvConfirmOpening.setText(EmptyUtils.strEmpty(btnText));

        tvConfirmOpening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iNoPayPwd.ConfirmOpening();
            }
        });

        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iNoPayPwd.cancel();
            }
        });

        noPayPwdBottomDialog = new BottomPop(context, bottomChooseLayout);
        noPayPwdBottomDialog.showPopupWindow();
    }

    //关闭dialog
    public void dismissNoPayPwdDialog() {
        if (noPayPwdBottomDialog != null && noPayPwdBottomDialog.isShowing()) {
            noPayPwdBottomDialog.dismiss();
            noPayPwdBottomDialog = null;
        }
    }

    public interface INoPayPwd {
        void ConfirmOpening();

        void cancel();
    }
    //-------------------------------------------弹出底部框  end-------------------------------------------------


    //-------------------------------------------弹出底部框  start-------------------------------------------------
    public void showCustomerDialog(View contentView, String title) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View bottomChooseLayout = inflater.inflate(R.layout.layout_customer_dialog, null);
        final TextView tvTitle = bottomChooseLayout.findViewById(R.id.tv_title);
        final FrameLayout flContain = bottomChooseLayout.findViewById(R.id.fl_contain);

        if (EmptyUtils.isEmpty(title)) {
            tvTitle.setVisibility(View.GONE);
        } else {
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(EmptyUtils.strEmpty(title));
        }

        if (flContain.getChildCount() > 0) {
            flContain.removeAllViews();
        }
        flContain.addView(contentView);

        customerBottomDialog = new BottomPop(context, bottomChooseLayout);
        customerBottomDialog.showPopupWindow();
    }

    //关闭dialog
    public void dismissCustomerDialog() {
        if (customerBottomDialog != null && customerBottomDialog.isShowing()) {
            customerBottomDialog.dismiss();
            customerBottomDialog = null;
        }
    }
    //-------------------------------------------弹出底部框  end-------------------------------------------------
}
