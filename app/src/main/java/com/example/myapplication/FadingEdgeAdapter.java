package com.example.myapplication;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/6/19 0019
 * 描    述：
 * ================================================
 */
public class FadingEdgeAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public FadingEdgeAdapter() {
        super(R.layout.layout_fadingedge_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, String item) {
//        TextView tv=helper.getView(R.id.tv);
//        tv.setText(item);
    }
}
