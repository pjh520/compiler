package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import com.frame.compiler.widget.CustomSafeKeyboard;
import com.frame.compiler.widget.MNPasswordEditText;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.ui.user.activitys
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/2/23 0023
 * 描    述：设置支付密码
 * ================================================
 */
public class SetPayPwdActivity extends BaseActivity implements SetPaypwdUtils.IComplete {
    MNPasswordEditText pwdInputview;
    CustomSafeKeyboard viewKeyboard;

    private SetPaypwdUtils setPaypwdUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_set_pay_pwd;
    }

    @Override
    public void initView() {
        super.initView();
        pwdInputview=findViewById(R.id.pwd_inputview);
        viewKeyboard=findViewById(R.id.view_keyboard);
    }

    @Override
    public void initData() {
        super.initData();
        setPaypwdUtils=new SetPaypwdUtils(pwdInputview,viewKeyboard,this);
    }

    @Override
    public void onComplete(String text) {
        showWaitDialog();
        setPwdRequest(text);
    }

    //设置支付密码
    private void setPwdRequest(String pwd) {

    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SetPayPwdActivity.class);
        context.startActivity(intent);
    }
}
