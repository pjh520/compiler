package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.frame.compiler.widget.FadingEdgeRecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FadingEdgeRecyclerView frlList = findViewById(R.id.frl_list);
        frlList.setHasFixedSize(true);
        frlList.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        FadingEdgeAdapter fadingEdgeAdapter=new FadingEdgeAdapter();
        frlList.setAdapter(fadingEdgeAdapter);

        ArrayList<String> datas=new ArrayList<>();
        for (int i=0;i<20;i++){
            datas.add(""+i);
        }

        fadingEdgeAdapter.setNewData(datas);
//        tv.setOnClickListener(new OnMultiClickListener() {
//            @Override
//            public void onNoDoubleClick(View v) {
//                SetPayPwdActivity.newIntance(MainActivity.this);
//            }
//        });
    }
}